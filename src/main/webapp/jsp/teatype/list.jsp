<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        
        <title>teatypelist</title>
    </head>
    <body>
        <h1>Tekintsd meg a teafajtáinkat. </h1>
        
    <p:>
        <form action="majd egy népszerűségi listaféle lesz" method="POST">
            Teafajták:<br/>
            <select name="type">
                <c:forEach items="${types}" var="type">
                    <option value="${type.teaName}">${type.teaName}</option>
                </c:forEach>
            </select>
            <input type="submit" value="statisztikák">
        </form>
    </p:>
            
    <p:>Ha úgy érzed a lista nem teljes, kérjük küld be a javaslatod.
        <br/>
        
        <form action="${pageContext.request.contextPath}/persisttype" method="POST">
            <input type="text" name="newtype"/>
            <input type="submit" value="javaslat"/>
            
        </form>
    
    </body>
</html>
