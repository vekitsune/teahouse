<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <title>notes</title>
    </head>

    <body>
        <h1>Böngészés a jegyzetek között:</h1>
        <h3>Lefuttatott query:</h3>

        <form action="list" method="post">
            Válassz egy jegyzetet a címe alapján!
            <select name="selectednote">
                <c:forEach items="${notes}" var="note">
                    <option value="${note.id}">${note.teaNote()}</option>
                </c:forEach>
            </select>
            <br/><br/>
            <input type="submit" value="Submit" />

        </form> 
        <jsp:include page="../footer.jsp"/>

    </body>
</html>
