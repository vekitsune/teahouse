<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <title>new note</title>
    </head>
    <body>
        <h1>Új jegyzet létrehozása</h1>
        <form action="${pageContext.request.contextPath}/createnote" method="post">
            Válaszd ki a teatípust!
            <select name="teatype">
                <c:forEach items="${teatypes}" var="teatype">
                    <option value="${teatype.id}">${teatype.teaName}</option>
                </c:forEach>
            </select><br/><br />
            Add meg a tea nevét!
            <input name="teaname"/><br /><br />
            Hány fokos vizet használtál?
            <input name="watertemp" type="number" min="0" max="100"/><br/><br />
            Értékelésed szerint hány pontos volt ez a tea?

            <input name ="score" type="number" min="1" max="100"/><br/><br />
            A felhasznált víz ásványianyagtartalma:
            <input name="mineral" type="number" min="10" max="200"/>mg/l<br/><br />
            Van kedved részletes jegyzetet írni?
            <input  name="note" type="textarea" maxlength="250" /><br /><br />
            <input type="submit" value="rögzítés"/><br /><br />
           


        </form>
            <br /><br /><br />
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
