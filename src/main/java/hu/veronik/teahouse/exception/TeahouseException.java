/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.teahouse.exception;

/**
 *
 * @author User
 */
public class TeahouseException extends Exception {
    public static final String WRONG_LOGIN="A felhaszn�l�n�v, jelsz� p�ros nem l�tezik";
    public static final String UNKNOWN="V�ratlan probl�ma!";
    
    public synchronized Throwable fillInStackTrace(){
        return this;
    }
    
    public TeahouseException(String message){
        super(message);
    }
    
}
