
package hu.veronik.teahouse.exception;


public class AuthException extends TeahouseException{
    
    public AuthException() {
        super(TeahouseException.WRONG_LOGIN);
    }
    
}
