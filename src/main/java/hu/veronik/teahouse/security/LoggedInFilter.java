
package hu.veronik.teahouse.security;

import hu.veronik.teahouse.security.LoginUtil;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;


public class LoggedInFilter implements Filter{
    private static Logger log=Logger.getLogger(LoggedInFilter.class);

    @Override
    public void init(FilterConfig fc) throws ServletException {
       
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
       HttpServletRequest httpServletRequest = (HttpServletRequest) sr;
        String user = (String) httpServletRequest.getSession().getAttribute(LoginUtil.ATTR_LOGGED_IN);
        if(user == null){
            log.warn("security filter redirect...");
            String s = sr.getServletContext().getContextPath()+"/login";
            log.info(s);
            HttpServletResponse httpResponse = (HttpServletResponse) sr1;
            httpResponse.sendRedirect(sr.getServletContext().getContextPath()+"/login");
        }
        fc.doFilter(sr, sr1);
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
