/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.log.LogUtil;
import hu.veronik.teahouse.security.LoginUtil;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class LoginServlet extends HttpServlet {
    private static Logger log=Logger.getLogger(LoginServlet.class);
    @EJB
    private IDataManager dataManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       log.info(LogUtil.METHOD_ENTER.concat("doGet"));
       req.getRequestDispatcher("/jsp/login/init.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       log.info(LogUtil.METHOD_ENTER.concat("doPost"));
       try{
           String user=req.getParameter("user");
           String password= req.getParameter("password");
           
           dataManager.authenticate(user, password);
           req.getSession().setAttribute(LoginUtil.ATTR_LOGGED_IN, user);
           
       }catch(Exception e){
           resp.sendRedirect(req.getContextPath()+"/login");
           return;
       }
       log.info(LogUtil.METHOD_SUCCESS.concat("POST"));
       req.getRequestDispatcher("/jsp/startpage/menu.jsp").forward(req, resp);
    }
    
    
    
}





   
