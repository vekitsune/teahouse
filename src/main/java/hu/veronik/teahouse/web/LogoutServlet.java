/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jboss.logging.Logger;



public class LogoutServlet extends HttpServlet {
private static Logger log=Logger.getLogger(LogoutServlet.class);

@EJB
private IDataManager dataManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       HttpSession session=req.getSession();
       session.invalidate();
       req.getRequestDispatcher("/jsp/startpage/menu.jsp").forward(req, resp);
    }

   
  

}
