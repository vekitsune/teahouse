
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.entity.TeanoteEntity;
import hu.veronik.teahouse.log.LogUtil;
import hu.veronik.teahouse.security.LoginUtil;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;

/**
 *
 * @author User
 */
public class NotesListServlet extends HttpServlet{
    private static Logger log=Logger.getLogger(NotesListServlet.class);
    
    @EJB
    private IDataManager dataManager;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       log.info(LogUtil.METHOD_ENTER.concat("GET"));
      
       try{
          
           
           List<TeanoteEntity> notes=dataManager.getFullList(TeanoteEntity.class);
           req.setAttribute("notes", notes);
       }catch(Exception e){
           req.setAttribute("result message", e.getMessage());
           e.printStackTrace();
       }
       log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
       req.getRequestDispatcher("/jsp/notes/browser.jsp").forward(req, resp);
    }
    
    
    
}
