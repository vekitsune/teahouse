package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.entity.TeatypeEntity;
import hu.veronik.teahouse.log.LogUtil;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jboss.logging.Logger;

/**
 *
 * @author User
 */
public class TypeListServlet extends HttpServlet{
    private static Logger log=Logger.getLogger(TypeListServlet.class);
    
    @EJB
    private IDataManager dataManager;
     
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException{
        
        
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        try{
            List<TeatypeEntity> types=dataManager.getFullList(TeatypeEntity.class);
            req.setAttribute("types", types);
        }catch(Exception e){
            e.printStackTrace();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("GET"));
        req.getRequestDispatcher("/jsp/teatype/list.jsp").forward(req, resp);
    }
     @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newtype=(String)req.getAttribute("newtype");
         boolean exist=false;
        
        List<TeatypeEntity> types=dataManager.getFullList(TeatypeEntity.class);
       
        for (TeatypeEntity type : types) {
            if(type.getTeaName().equalsIgnoreCase(newtype)){
                exist=true;
            }
        
        }
        if(!exist&&newtype!=null){
            TeatypeEntity newt=new TeatypeEntity(newtype);
            dataManager.teytypePersist(newtype);
        }
        req.getRequestDispatcher("/jsp/teatype/list.jsp").forward(req, resp);
        
    }
    
}
