/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.entity.TeanoteEntity;
import hu.veronik.teahouse.entity.TeatypeEntity;
import hu.veronik.teahouse.log.LogUtil;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author User
 */
public class CreateNoteServelt extends HttpServlet{
    private static Logger log=Logger.getLogger(CreateNoteServelt.class);
     @EJB
     IDataManager dataManager;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("POST"));
        try{
            
            
      //  (Integer teatypeId, Integer loggedinUser, String teaName, Integer watertemp,Integer score, Integer minarel, String note);
           
            Integer teatypeId=1;
 
            Integer loggedinUser=1;
            String teaName=req.getParameter("teaname");
            Integer watertemp=Integer.parseInt(req.getParameter("watertemp"));
            Integer score=Integer.parseInt(req.getParameter("score"));
            Integer mineral=Integer. parseInt(req.getParameter("mineral"));
            String note=req.getParameter("note");
            TeanoteEntity thisnote=new TeanoteEntity(loggedinUser, teaName, teatypeId, score, note, watertemp, mineral);
           
                    
            
            dataManager.notePersist(thisnote);
            //req.setAttribute("result_message", "A jegyzet elmentése megtörtént");
            
        
                 
            
        }catch(Exception e){
            e.printStackTrace();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("POST"));
        req.getRequestDispatcher("/jsp/notes/result").forward(req, resp);
    }
     
     
}
