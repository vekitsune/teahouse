/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.entity.TeatypeEntity;
import hu.veronik.teahouse.log.LogUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;


public class NewNoteServlet extends HttpServlet {
    private static Logger log=Logger.getLogger(NewNoteServlet.class);
    @EJB
    private IDataManager dataManager;
  

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info(LogUtil.METHOD_ENTER.concat("GET"));
        List<TeatypeEntity> teatypes = dataManager.getFullList(TeatypeEntity.class);
        
         String test = "";
           
            request.setAttribute("teatypes", teatypes);
        request.getRequestDispatcher("/jsp/notes/newnote.jsp").forward(request, response);
       
    }

   
   

  
}
