
package hu.veronik.teahouse.web;

import hu.veronik.teahouse.ejb.IDataManager;
import hu.veronik.teahouse.entity.TeatypeEntity;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PersistTeatypeServlet extends HttpServlet{
@EJB
private IDataManager dataManager;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newtype=(String)req.getAttribute("newtype");
         boolean exist=false;
        
        List<TeatypeEntity> types=dataManager.getFullList(TeatypeEntity.class);
       
        for (TeatypeEntity type : types) {
            if(type.getTeaName().equalsIgnoreCase(newtype)){
                exist=true;
            }
        
        }
        if(!exist&&newtype!=null){
            TeatypeEntity newt=new TeatypeEntity(newtype);
            dataManager.teytypePersist(newtype);
        }
        req.getRequestDispatcher("/jsp/teatype/list.jsp").forward(req, resp);
        
    }
    
}
