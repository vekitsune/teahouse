package hu.veronik.teahouse.ejb;

import hu.veronik.teahouse.entity.TeanoteEntity;
import hu.veronik.teahouse.entity.TeatypeEntity;
import hu.veronik.teahouse.entity.UserEntity;
import hu.veronik.teahouse.exception.AuthException;
import hu.veronik.teahouse.exception.TeahouseException;
import hu.veronik.teahouse.log.LogUtil;
import java.util.Date;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.*;

import org.apache.log4j.Logger;

@Stateless
public class DataManager implements IDataManager {

    private static Logger log = Logger.getLogger(DataManager.class);

    @PersistenceContext(unitName = "teatime")
    private EntityManager entityManager;
// az eddigi helyeken m�rt nem haszn�lom az entityManager ny�jtotta el�ny�ket?

    public String getQueryString() {
        String queryString = null;
        Table tableAnnotation = TeanoteEntity.class.getAnnotation(Table.class);
        if (tableAnnotation != null) {
            queryString = "SELECT t FROM ".concat(tableAnnotation.name()).concat(" t");
        }
        return queryString;
    }

    @Override
    public <T> List<T> getFullList(Class<T> klass) {
        log.info(LogUtil.METHOD_ENTER.concat("getFullList()"));
        Table tableAnnotation = klass.getAnnotation(Table.class);
        String queryString = null;
        if (tableAnnotation != null) {
            queryString = "SELECT t FROM ".concat(klass.getSimpleName()).concat(" t");
            log.debug("query: ".concat(queryString));
            TypedQuery<T> query = (TypedQuery<T>) entityManager.createQuery(queryString);
            log.info(LogUtil.METHOD_SUCCESS.concat("getFullList()"));
            return query.getResultList();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("getFullList"));
        return null;
    }

    @Override
    public <T> List<T> getFilterList(Class<T> klass) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserEntity getUser(String userName) {
        //mivel csak n�v alapj�n keres, vagy kiz�rjuk regisztr�l�sn�l a m�r l�tez� nickv�laszt�s�t
        //�s ezzel egy�tt az adatb�zisban megadjuk hogy login unique
        //vagy egy ak�r t�bb elem� list�t k�ne visszaadnia
        //T�telezz�k fel, hogy nem enged�nk m�r l�tez� n�vvel �jabb regisztr�ci�t
        log.info(LogUtil.METHOD_ENTER.concat("getUser"));

        UserEntity re = null;
        try {
            TypedQuery<UserEntity> query = entityManager.createNamedQuery(UserEntity.QUERY_FIND_BY_LOGIN, UserEntity.class);
            query.setParameter(UserEntity.PARAM_LOGIN, userName);
            List<UserEntity> resultList = query.getResultList();
            if (!resultList.isEmpty()) {
                re = resultList.get(0);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info(LogUtil.METHOD_SUCCESS.concat("getUser"));
        return re;

    }

    @Override
    public void authenticate(String user, String password) throws TeahouseException {
        log.info(LogUtil.METHOD_ENTER.concat("authentication"));

        UserEntity userEntity = null;
        userEntity = getUser(user);
        if (userEntity == null || !userEntity.getPassword().equals(password)) {
            throw new AuthException();
        }
    }
    // A 22sorban tett megjegyz�st�l id�ig,... ma m�r m�sk�pp csin�ln�m.

    public void notePersist(Integer teatypeId, Integer loggedinUser, String teaName, Integer watertemp, Integer score, Integer minarel, String note) {

        TeanoteEntity newTeaNote = new TeanoteEntity();
        newTeaNote.setTeatypeId(teatypeId);
        newTeaNote.setCreateUserId(loggedinUser);
        newTeaNote.setCreateDate(new Date());
        newTeaNote.setMineral(minarel);
        newTeaNote.setModifyDate(new Date());
        newTeaNote.setNote(note);
        newTeaNote.setScore(score);
        newTeaNote.setTeaName(teaName);
        newTeaNote.setWaterTemp(watertemp);

        entityManager.persist(newTeaNote);

        log.info(LogUtil.METHOD_SUCCESS.concat("notePersist()"));

    }

    @Override
    public void notePersist(TeanoteEntity thisnote) {
        entityManager.persist(thisnote);
    }

    @Override
    public void teytypePersist(String name) {
        TeatypeEntity newtype=new TeatypeEntity(name);
       entityManager.persist(newtype);
    }
    

    public Integer getTeataypeId(String teatypeName) {

        return entityManager.find(TeatypeEntity.class, teatypeName).getId();

    }

}
