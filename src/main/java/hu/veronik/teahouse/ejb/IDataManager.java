package hu.veronik.teahouse.ejb;

import hu.veronik.teahouse.entity.TeanoteEntity;
import hu.veronik.teahouse.entity.TeatypeEntity;
import hu.veronik.teahouse.entity.UserEntity;
import hu.veronik.teahouse.exception.TeahouseException;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author User
 */
@Local
public interface IDataManager {

    UserEntity getUser(String userName);

    //Használhatnám a teatypeName Stringet a notePersist függvényben, de lehet jobb ha mindig
    //visszatérek az Entitykhez ?
    Integer getTeataypeId(String teatypeName);

    void authenticate(String user, String password) throws TeahouseException;

    <T> List<T> getFullList(Class<T> klass);

    <T> List<T> getFilterList(Class<T> klass);

    public String getQueryString();

    void notePersist(Integer teatypeId, Integer loggedinUser, String teaName, Integer watertemp, Integer score, Integer minarel, String note);

    void notePersist(TeanoteEntity thisnote);
    void teytypePersist(String name);
}
