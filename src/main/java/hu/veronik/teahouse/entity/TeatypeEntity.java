package hu.veronik.teahouse.entity;

import java.io.Serializable;
import javax.persistence.*;

@Table(name = "teatype")

@NamedQueries({
    @NamedQuery(name=TeatypeEntity.QUERY_FIND_BY_NAME,
            query = "SELECT u FROM TeatypeEntity u WHERE u.teaName=:"+TeatypeEntity.PARAM_NAME)
                    
})
@Entity
public class TeatypeEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_NAME = "Currency.findByTeaName";
    public static final String PARAM_NAME = "name";

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;
    
   
    
    @Column(name = "name")
    private String teaName;

    public TeatypeEntity() {
    }

    public TeatypeEntity(String teaName) {
        this.teaName = teaName;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

   
   

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    @Override
    public String toString() {
        return "TeatypeEntity{" + "id=" + id + ", teaName=" + teaName + '}';
    }
    
    

}
