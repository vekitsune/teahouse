package hu.veronik.teahouse.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "teanote")

@NamedQueries({
    @NamedQuery(name = TeanoteEntity.QUERY_FIND_BY_ID,
            query = "SELECT t FROM TeanoteEntity t WHERE t.id=:" + TeanoteEntity.PARAM_ID)
       
}
)

@Entity
public class TeanoteEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_ID = "TeanoteEntity.findById";
    public static final String PARAM_ID = "id";

//    public static final String QUERY_FIND_BY_CREATEDATE = "TeanoteEntity.findByCreateDate";
//    public static final String PARAM_CREATE_DATE = "createDate";

    public TeanoteEntity() {
    }

    public TeanoteEntity( Integer createdById, String teaName, Integer teatypeId, Integer score, String note, Integer waterTemp, Integer mineral) {
        this.teaName = teaName;
        this.teatypeId = teatypeId;
        this.score = (score==null)?-1:score;
        this.note = (note==null)?"M�g nincs megjegyz�s":note;
        this.waterTemp = (waterTemp==null)?-1:waterTemp;
        this.mineral = (mineral==null)?-1:mineral;
        this.createDate=new Date();
        this.modifyDate=createDate;
        this.createUserId=createdById;
    }
    
    
   
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id ")
    private Integer id;

    @Column(name = "create_user_id")
   // @OneToMany
    //@JoinColumn(name="user.id")
    
    private Integer createUserId;

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }

    public void setTeatypeId(Integer teatypeId) {
        this.teatypeId = teatypeId;
    }

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = " create_date")
    private Date createDate;

    @Column(name = "tea_name")
    private String teaName;

    @Column(name = "teatype_id ")
    private Integer teatypeId;

    @Column(name = "score ")
    private Integer score;
    
    @Column(name = "note")
    private String note;
    
    @Column(name = "water_temp")
    private Integer waterTemp;
    
    @Column(name = "water_mineral")
    private Integer mineral;
    
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date modifyDate;

   

  

   

   
    public Integer getId() {
        return id;
    }

    public Integer getCreateUserId() {
        return createUserId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getTeaName() {
        return teaName;
    }

    public Integer getTeatypeId() {
        return teatypeId;
    }

    public Integer getScore() {
        return score;
    }

    public String getNote() {
        return note;
    }

    public Integer getWaterTemp() {
        return waterTemp;
    }

    public Integer getMineral() {
        return mineral;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setWaterTemp(Integer waterTemp) {
        this.waterTemp = waterTemp;
    }

    public void setMineral(Integer mineral) {
        this.mineral = mineral;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "TeanoteEntity{" + "id=" + id + ", createUserId=" + createUserId + ", createDate=" + createDate + ", teaName=" + teaName + ", teatypeId=" + teatypeId + ", score=" + score + ", note=" + note + ", waterTemp=" + waterTemp + ", mineral=" + mineral + ", modifyDate=" + modifyDate + '}';
    }

    
    

}
