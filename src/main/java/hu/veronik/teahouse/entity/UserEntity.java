
package hu.veronik.teahouse.entity;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "user")
@NamedQueries({
        @NamedQuery(name=UserEntity.QUERY_FIND_BY_LOGIN,
                query="SELECT u FROM UserEntity u WHERE u.login=:"+UserEntity.PARAM_LOGIN)
})
@Entity
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String QUERY_FIND_BY_LOGIN = "UserEntity.findByLogin";
    public static final String PARAM_LOGIN         = "login";

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserEntity{" + "id=" + id + ", login=" + login + '}';
    }
    
    
}
