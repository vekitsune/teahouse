CREATE DATABASE if not exists`teanotes` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE if not exists `teatype` (
  `id` int  NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
CREATE TABLE if not exists`user` (
  `id` int  NOT NULL AUTO_INCREMENT,
  `login` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
CREATE TABLE if not exists`teanote` (
`id`  int  NOT NULL AUTO_INCREMENT,
` create_user_id`  int not null,
`create_date` date not null,
`tea_name` varchar(25) not null,
`teatype_id`  int not null,
`score` int not null,
`note` varchar(800),
`water_temp` int not null,
`water_mineral` int not null,
`modify_date` date not null,
PRIMARY KEY (`id`),
FOREIGN KEY (` create_user_id`) REFERENCES user(`id`),

foreign key(`teatype_id`) references teatype(`id`)

)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;
